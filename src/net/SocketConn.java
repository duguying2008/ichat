/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import message.Receive;


/**
 * Socket连接类
 * @author rex
 */
public class SocketConn {
    //Socket对象
    private Socket skt;
    //服务器ServerSocket对象
    public static ServerSocket serv_skt;
    //输出缓冲区，供发送
    private OutputStream ops;
    //输入缓冲区，供接收
    private InputStream ips;
    //ip
    private String ip;
    //port
    private int port;
    /**
     * 客户端连接
     * @param ip
     * @param port 
     */
    public SocketConn(String ip, int port){
        this.ip=ip;
        this.port=port;
    }
    /**
     * 服务器连接
     * @param port 
     */
    public SocketConn(int port){
        this.port=port;
    }
    public void client() throws IOException{
        this.skt = new Socket(this.ip,this.port,true);
        //设置缓冲区
        skt.setReceiveBufferSize(1024);
        skt.setSendBufferSize(1024);
        //从Socket对象中获取缓冲区
        ops=skt.getOutputStream();
        ips=skt.getInputStream();
    }
    
    /**
     * 通过String获取InetSocketAddress
     * @return
     * @throws UnknownHostException 不合法String抛出异常 
     */
    public static InetSocketAddress address(String host,int port) throws UnknownHostException{
        InetSocketAddress addr= new InetSocketAddress(host,port);
        return addr;
    }
    /**
     * 作为服务器监听
     */
    public void listen() throws IOException{
        SocketConn.serv_skt=new ServerSocket(this.port);
        this.skt=SocketConn.serv_skt.accept();//TODO 此处应当启用多线程
        //设置缓冲区
        skt.setReceiveBufferSize(1024);
        skt.setSendBufferSize(1024);
        //从Socket对象中获取缓冲区
        ops=skt.getOutputStream();
        ips=skt.getInputStream();
        //监听接受信息
        Thread lis=new Thread(Receive.init(this));
        lis.start();
    }
    /**
     * 初始化Socket,并作为客户端连接
     */
    public static SocketConn init(String ip, int port) throws IOException{
        SocketConn client_skt = new SocketConn(ip, port);
        client_skt.client();
        return client_skt;
    }
    /**
     * 初始化Socket,并作为服务器端连接
     */
    public static SocketConn serv_init(int port) throws IOException{
        SocketConn client_skt = new SocketConn(port);
        client_skt.listen();
        return client_skt;
    }
    /**
     * 接收信息，注意！此方法会产生阻塞，请使用多线程
     * @return string 收到的信息
     * @throws IOException 
     */
    public String recv() throws IOException{
        //创建数据暂存缓冲去
        byte[] buffer= new byte[1000];
        //将从InputStream中读取的数据存入缓冲区
        this.ips.read(buffer);
        //转化为String
        return new String(buffer).trim();
    }
    /**
     * 发送
     */
    public SocketConn send(String message) throws IOException{
        //如果message为null，置空串
        if(message==null){
            message="";
        }
        //信息写入缓冲区，发送
        this.ops.write(message.getBytes());
        //返回本对象，保持对象连贯性
        return this;
    }
    
    /**
     * 判断服务器是否已经关闭
     * @return 
     */
    public boolean is_close(){
        boolean rst=false;
        try{
                this.skt.sendUrgentData(0xFF);
          }catch(Exception ex){
            rst = true;
          }
        //boolean connected = this.skt.isConnected();
        return rst;
    }
    

}
