/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package message;

import java.awt.Color;
import java.awt.Component;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import net.SocketConn;
import window.ChatMain;

/**
 *
 * @author rex
 */
public class Receive implements Runnable{
    //传入的SocketConn对象
    public SocketConn cskt;
    
    @Override
    public void run() {
        for(;true;){
            try {
                String msg=this.cskt.recv();
                //System.out.print(msg);
                ChatMain.form.putMsg2Box(msg,"TA", Color.RED);
                //如果连接已经关闭
                if(cskt.is_close()){
                    JOptionPane.showMessageDialog(null, "连接已经断开！");
                    //获取当前线程
                    Thread ct=Thread.currentThread();
                    //结束线程
                    ct.destroy();
                }
            } catch (IOException ex) {
                Logger.getLogger(Receive.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    /**
     * 初始化Receive对象
     * @param cskt
     * @return 
     */
    public static Receive init(SocketConn cskt){
        Receive rcv=new Receive();
        rcv.cskt=cskt;
        return rcv;
    }
    
}
