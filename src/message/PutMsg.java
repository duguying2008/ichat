/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package message;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JTextPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

/**
 *
 * @author rex
 */
public class PutMsg {
    JTextPane   textPane; 

    public   PutMsg(JTextPane jtp){ 
        textPane   =   jtp; 
    } 

    public   void   insert(String   str,   AttributeSet   attrSet)   { 
        Document   doc   =   textPane.getDocument(); 
        str = str+'\n' ; 
        try   { 
            doc.insertString(doc.getLength(),   str,   attrSet); 
        } 
        catch   (BadLocationException   e)   { 
            System.out.println( "BadLocationException:   "   +   e); 
        } 
    } 

    public   void   setDocs(String   str,Color   col,boolean   bold,int   fontSize)   { 
        SimpleAttributeSet   attrSet   =   new   SimpleAttributeSet(); 
        StyleConstants.setForeground(attrSet,   col); 
        //颜色 
        if(bold==true){ 
            StyleConstants.setBold(attrSet,   true); 
        }//字体类型 
        StyleConstants.setFontSize(attrSet,   fontSize); 
        //字体大小 
        insert(str,   attrSet); 
    } 

//    public   void   gui()   { 
//        //textPane.insertIcon(image);   //插入图片 
//        setDocs( "第一行的文字文字文字文字文字文字文字文字文字文字 ",Color.red,false,20);   //折行测试
//        setDocs( "第二行的文字 ",Color.BLACK,true,25); 
//        setDocs( "第三行的文字 ",Color.BLUE,false,20); 
//        frame.getContentPane().add(textPane,   BorderLayout.CENTER); 
//        frame.addWindowListener(new   WindowAdapter()   { 
//            public   void   windowClosing(WindowEvent   e)   { 
//                System.exit(0); 
//            }}); 
//        frame.setSize(200,300); 
//        frame.setVisible(true); 
//    } 
     
}
