/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package message;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rex
 */
public class Charset {
    public static String utf8Togb2312(String str){   
        //String urlEncode = "" ;   
        try { 
            str = URLEncoder.encode (str, "UTF-8" );
            str = URLDecoder.decode(str, "GB2312");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Charset.class.getName()).log(Level.SEVERE, null, ex);
        }
        return str;
  
    }  
  
  
    //GB2312->UTF-8  
    public static String gb2312ToUtf8(String str) {   
        try {
            String urlEncode = "" ;   
            urlEncode = URLEncoder.encode (str, "UTF-8" ); 
            return urlEncode;   
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Charset.class.getName()).log(Level.SEVERE, null, ex);
        }
        return str;
  
    }  
}
